const express = require('express');
const fs = require('fs');
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
const port = 3000;

const manejaGet = (req, res) => {
    console.log("Estoy en la pagina home");
    res.json({saludo: 'Hola mundo desde Node!'});
}
app.get('/home', manejaGet);

const manejaGet2 = (req, res) => {
    console.log("Estoy en la pagina de Cristian Chipol Escribano");
    res.json({Nombre: 'Cristian Chipol Escribano'});
}
app.get('/cristian', manejaGet2);

const readFile = (err, data) => {
    if(err) console.log('Hubo un error');
    let info = JSON.parse(data);
    console.log(info);
    return info;
}

app.get('/people', (req, res)=> {
    let data = fs.readFileSync('database/table.json');
    let info = JSON.parse(data);
    console.log('Leimos el archivo',info);
    res.json({response: '[]'})
})

//Mostrar Personas
app.get('/people/:id', (req, res)=>{
    let identifier = req.params.id;
    let data = fs.readFileSync('database/table.json');
    let obJSON = JSON.parse(data);

    let response = null;
    let aux;
    for(let i=0; i<obJSON.length; i++){
        if(obJSON[i].id==identifier){
            response = obJSON[i];
            aux = obJSON[i];
            break;
        }
    }
//Verifica si encontro la persona o tal vez no.
    if(response==null){
        res.status(404).send();
        return;
    }
    console.log('Dato: ',aux);
    res.json(response);
})

//Agregar Persona.
app.post('/people', (req, res)=>{
    console.log(req.body);
    let person = req.body;
    let data = fs.readFileSync('database/table.json');
    let info = JSON.parse(data);

    for(let i=0; i<info.length; i++){
        if(info[i].id==person.id){
            res.status(400).json({mensaje: 'El usuario ya existe'});   
            return;
        }
    }
    info.push(person);
    fs.writeFileSync('database/table.json', JSON.stringify(info));
    res.status(201).json(person);
})

app.listen(port, () => {
    console.log("El servidor esta escuchando en el puerto url https://localhost:", port);
});